/** @type {import('next').NextConfig} */
/**
const nextConfig = {}

module.exports = nextConfig
*/
// To add a proxy to an API
module.exports = {
    images: {
        formats: ['image/webp'],
        remotePatterns: [
            {
                protocol: 'http',
                hostname: '127.0.0.1',
                port: '3000',
                pathname: '/api/ER/**',
            },
        ],
    },
    async rewrites() {
        return [
            {
                source: '/api/:path*',
                destination: 'http://127.0.0.1:8000/api/:path*', // Proxy to Backend
            },
        ]
    },
}
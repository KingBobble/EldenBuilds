'use client'
import Image from 'next/image'
import Link from 'next/link'
import { useEffect, useState } from 'react'
import { TrackCartNum } from './helper'

const cart_logo = "/cart.png"
const home_logo = "/home.png"

export default function Nav({ cartStatus }) {
  const [cartNumber, setCartNumber] = useState(null)

  useEffect(() => {
    if (typeof window !== 'undefined' && window.localStorage) {
      let cartData = JSON.parse(localStorage.getItem('CART_STATE'))
      let cartNumber = 0
      if (cartData != null) {
        cartNumber = TrackCartNum(cartData)
      }
      setCartNumber(cartNumber)
    }
  }, [cartStatus])

  return (
    <div className="z-10 w-full max-w-5xl items-center justify-between font-mono text-sm lg:flex">
      <p className="fixed left-0 top-0 w-full flex justify-center border-b bg-gradient-to-b pb-6 pt-8 backdrop-blur-2xl border-neutral-800 bg-zinc-800/30 from-inherit lg:static lg:w-auto lg:rounded-xl lg:border lg:p-4 lg:bg-zinc-800/30">
        <Link href="/">
          <Image
            className="shrink-0 w-auto h-auto"
            src={home_logo}
            alt='Home_Logo'
            width={20}
            height={0}
          />
        </Link>
        &nbsp;Welcome to the EldenBuilds Store
      </p>

      <div className="fixed bottom-0 left-0 flex h-48 w-full items-end justify-center bg-gradient-to-t from-black via-black lg:static lg:h-auto lg:w-auto lg:bg-none">
        <div className="flex place-items-center gap-2 p-8 lg:pointer-events-auto lg:p-0">
          Cart
          <Link href="/cart">
            <Image
              className="w-auto h-auto"
              src={cart_logo}
              alt='Cart_Logo'
              width={40}
              height={0}
            />
          </Link>
          {cartStatus ? cartStatus : cartNumber}
        </div>
      </div>
    </div>
  )
}
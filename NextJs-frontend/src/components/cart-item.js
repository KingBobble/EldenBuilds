'use client'
import { TrackCartNum } from './helper'

export default function CartItem({ item, itemKey, removeItem, updateList }) {

    function removeCartItem() {
        if (typeof window !== "undefined" && window.localStorage) {
            let cartData = JSON.parse(localStorage.getItem("CART_STATE"))
            let cartNumber = null
            if (cartData != null) {
                for (let category in cartData.CART_ITEMS) {
                    if (category == item.category) {
                        cartData.CART_ITEMS[category].map((i, index) => {
                            if (JSON.stringify(i) == JSON.stringify(item)) {
                                cartData.CART_ITEMS[category].splice(index, 1)
                            }
                        })
                        break
                    }
                }
                cartNumber = TrackCartNum(cartData)
                localStorage.setItem("CART_STATE", JSON.stringify(cartData))
                if (!cartNumber) {
                    cartNumber = null
                    localStorage.setItem("CART_STATE", null)
                }
            }
            removeItem(cartNumber)
            updateList(itemKey)
        }
    }

    return (
        <ul>
            <li className="relative rounded-md p-3">
                <h3 className="text-sm font-medium leading-5">
                    <div className="flex">
                        {item.category != "Weapons" ? item.name : item.customItemName}
                        <div className="flex-auto w-40">
                        </div>
                    </div>
                    <button onClick={removeCartItem} className="absolute inset-y-4 right-0 h-8 inline-flex justify-center rounded-md border border-transparent bg-blue-100 px-4 py-2 text-sm font-medium text-blue-900 hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2">
                        Remove
                    </button>
                </h3>

                <ul className="mt-1 flex space-x-1 text-xs font-normal leading-4 text-gray-300">
                    <li>{item.category}</li>
                    {item.category == item.type ? null :
                        <>
                            <li>&middot;</li>
                            <li>{item.type}</li>
                        </>}
                </ul>
            </li>
        </ul>
    )
}
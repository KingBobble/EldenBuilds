import Image from 'next/image'

const banner = "/app.png"

export default function Banner() {
    return (
        <div className="relative flex place-items-center before:absolute before:h-[300px] before:w-[480px] before:-translate-x-1/2 before:rounded-full before:blur-2xl before:content-[''] after:absolute after:-z-20 after:h-[180px] after:w-[240px] after:translate-x-1/3 after:bg-gradient-conic after:blur-2xl after:content-[''] before:bg-gradient-to-br before:from-transparent before:to-yellow-500 before:opacity-10 after:from-yellow-500 after:via-[#ffea01] after:opacity-40 before:lg:h-[360px] z-[-1]">
            <Image
                className="relative drop-shadow-[0_0_0.3rem_#ffffff70] h-auto w-auto"//dark:invert
                src={banner}
                alt="App_Logo"
                width={180}
                height={40}
                priority
            />
        </div>
    )
}

'use client'
import { useEffect, useState } from 'react'
import { Tab } from '@headlessui/react'
import CartItem from './cart-item'
import { TrackCartNum } from './helper'
import Image from 'next/image'

const copy_logo = "/copy.png"

export default function List({ removeItem }) {
    const [categories, setCategories] = useState(null)
    const [rowItem, setRowItem] = useState(null)
    var itemKey

    function removeCartItem(cartData) {
        if (rowItem != null && cartData != null) {
            cartData[rowItem[0]].filter((item) => item.id !== rowItem[1])
        }
    }

    useEffect(() => {
        if (typeof window !== 'undefined' && window.localStorage) {
            let cartData = JSON.parse(localStorage.getItem('CART_STATE'))
            let cartNumber = 0
            if (cartData != null) {
                cartNumber = TrackCartNum(cartData)
                if (cartNumber) {
                    cartData = cartData.CART_ITEMS
                } else {
                    cartData = null
                }
            }
            setCategories(cartData)
            removeCartItem(cartData)
        }

    }, [rowItem])

    function clearCart() {
        if (typeof window !== 'undefined' && window.localStorage) {
            localStorage.setItem("CART_STATE", null)
            removeItem(null)
            setCategories(null)
        }
    }

    function copyCart() {
        let cartText = ""

        if (categories != null) {
            Object.values(categories).map((items) => (
                items.map((item) => (
                    item.category != "Weapons" ? cartText += item.name + "\r\n" :
                        cartText += item.customItemName + "\r\n"
                ))
            ))
        }

        if (window.isSecureContext) {
            navigator.clipboard.writeText(cartText).then((x) => alert("Cart items sent to clipboard."))
        } else {
            const textarea = document.createElement('textarea');
            textarea.value = cartText;
            document.body.prepend(textarea);
            textarea.setSelectionRange(0, 99999)
            textarea.select();

            try {
                document.execCommand('copy');
            } catch (err) {
                console.log(err);
            } finally {
                textarea.remove();
                alert("Cart items sent to clipboard.")
            }
        }
    }

    return (
        <div className="w-full max-w-5xl px-2 py-16 sm:px-0">
            <div className="flex space-x-1 rounded-xl bg-yellow-500 p-1">
                <button type="button" onClick={copyCart} className="inline-flex flex-col justify-center rounded-md border border-transparent bg-slate-800 px-2 py-1 text-sm font-medium hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2">
                    Copy
                    <Image
                        className="w-auto h-auto"
                        src={copy_logo}
                        alt='Copy_Logo'
                        width={30}
                        height={0}
                    />
                </button>
                <p className="w-full grow text-center text-2xl">
                    Cart List
                </p>
                <button type="button" onClick={clearCart} className="inline-flex justify-center rounded-md border border-transparent bg-slate-800 px-0.5 py-1 text-sm font-medium hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2">
                    Clear Cart
                </button>
            </div>

            {categories == null ? null :
                <Tab.Group>
                    <Tab.Panels className="mt-2">
                        <Tab.Panel className={'rounded-xl bg-slate-800 p-3'}>
                            {categories == null ? null : Object.values(categories).map((posts) => (
                                posts.map((post, id) => (
                                    itemKey = [post.category, id + post.category + post.id],
                                    <CartItem
                                        key={itemKey[1]}
                                        itemKey={itemKey}
                                        item={post}
                                        removeItem={removeItem}
                                        updateList={setRowItem}
                                    />
                                ))
                            ))}
                        </Tab.Panel>
                    </Tab.Panels>
                </Tab.Group>
            }
        </div>
    )
}

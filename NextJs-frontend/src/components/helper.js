export function TrackCartNum(items) {
    const lengthOfVariants = Object.keys(items.CART_ITEMS).map(category => items.CART_ITEMS[category] ?
        items.CART_ITEMS[category].length : 0)
    const totalLength = lengthOfVariants.reduce((a, b) => a + b, 0);

    return totalLength
}

export function insertDecimalBy1(num) {
    return num.toFixed(1);
 }
'use client'
import Nav from '@/components/nav'
import Item from '@/components/item'
import { useState } from 'react'
import CategoryDropdown from '@/components/dropdown'

export default function Overview({ info, src, dropdownEnabled }) {
    const [cartStatus, setCartStatus] = useState(null)
    const [itemTypeSelected, setItemTypeSelected] = useState(!info.status ? null : info.data[0].category)

    return (
        <>
            <Nav cartStatus={cartStatus} />

            {!info.status ? null :
                <CategoryDropdown category={info.data[0].category}
                    dropdownEnabled={dropdownEnabled}
                    itemTypeSelected={setItemTypeSelected}
                />}

            {!info.status ? null :
                <div className="grid grid-cols-4 lg:grid-cols-8 gap-0.5" >
                    {info.data.map((item) => (
                        itemTypeSelected == item.category ?
                            <Item key={item.category + item.id}
                                item={item}
                                image_url={src}
                                cart_status={setCartStatus}
                            />
                            : itemTypeSelected == item.type ?
                                <Item key={item.category + item.id}
                                    item={item}
                                    image_url={src}
                                    cart_status={setCartStatus}
                                />
                                : null
                    ))}
                </div>
            }
        </>
    )
}
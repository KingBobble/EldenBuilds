'use client'
import { Dialog, Transition } from '@headlessui/react'
import { Fragment, useState } from 'react'
import Image from 'next/image'
import { TrackCartNum, insertDecimalBy1 } from './helper'
import { AffinityDropdown, LevelDropdown, AowDropdown } from './dropdown'

export default function Item({ item, image_url, cart_status }) {
  const [isOpen, setIsOpen] = useState(false)
  const [affinity, setAffinity] = useState("Standard")
  const [aow, setAow] = useState("None")
  const [level, setLevel] = useState("Base")

  function addToCart() {
    if (typeof window !== "undefined" && window.localStorage) {
      let cartData = JSON.parse(localStorage.getItem("CART_STATE"))
      let cartNumber = null

      if (cartData != null) {
        switch (item.category) {
          case "Weapons":
            if (affinity == "Standard") {
              if (level == "Base") {
                if (aow == "None") {
                  item["customItemName"] = item.name
                } else {
                  item["customItemName"] = item.name + " w/ " + aow
                }
              } else {
                if (aow == "None") {
                  item["customItemName"] = item.name + " " + level
                } else {
                  item["customItemName"] = item.name + " " + level + " w/ " + aow
                }
              }
            } else {
              if (level == "Base") {
                if (aow == "None") {
                  item["customItemName"] = affinity + " " + item.name
                } else {
                  item["customItemName"] = affinity + " " + item.name + " w/ " + aow
                }
              } else {
                if (aow == "None") {
                  item["customItemName"] = affinity + " " + item.name + " " + level
                } else {
                  item["customItemName"] = affinity + " " + item.name + " " + level + " w/ " + aow
                }
              }
            }
            cartData.CART_ITEMS.Weapons.push(item)
            break
          case "Talismans":
            cartData.CART_ITEMS.Talismans.push(item)
            break
          case "Spells":
            cartData.CART_ITEMS.Spells.push(item)
            break
          case "Materials":
            cartData.CART_ITEMS.Materials.push(item)
            break
          case "Consumables":
            cartData.CART_ITEMS.Consumables.push(item)
            break
          case "AoWs":
            cartData.CART_ITEMS.AoWs.push(item)
            break
          case "Armors":
            cartData.CART_ITEMS.Armors.push(item)
            break
        }
      } else {
        cartData = {
          CART_ITEMS: {
            Weapons: [], Talismans: [], Spells: [],
            Materials: [], Consumables: [], AoWs: [], Armors: []
          }
        }
        for (let category in cartData.CART_ITEMS) {
          if (category == item.category) {
            if (category == "Weapons") {
              if (affinity == "Standard") {
                if (level == "Base") {
                  if (aow == "None") {
                    item["customItemName"] = item.name
                  } else {
                    item["customItemName"] = item.name + " w/ " + aow
                  }
                } else {
                  if (aow == "None") {
                    item["customItemName"] = item.name + " " + level
                  } else {
                    item["customItemName"] = item.name + " " + level + " w/ " + aow
                  }
                }
              } else {
                if (level == "Base") {
                  if (aow == "None") {
                    item["customItemName"] = affinity + " " + item.name
                  } else {
                    item["customItemName"] = affinity + " " + item.name + " w/ " + aow
                  }
                } else {
                  if (aow == "None") {
                    item["customItemName"] = affinity + " " + item.name + " " + level
                  } else {
                    item["customItemName"] = affinity + " " + item.name + " " + level + " w/ " + aow
                  }
                }
              }
            }
            cartData.CART_ITEMS[category].push(item)
            break
          }
        }
      }

      cartNumber = TrackCartNum(cartData)
      localStorage.setItem("CART_STATE", JSON.stringify(cartData))
      cart_status(cartNumber)
    }
    setIsOpen(false)
  }

  function closeModal() {
    setIsOpen(false)
  }

  function openModal() {
    setIsOpen(true)
  }

  return (
    <>
      <div>
        <button
          type="button"
          onClick={openModal}
        >
          <Image
            src={image_url + item.src}
            alt={item.alt}
            width={200}
            height={200}
          />
          <p className="text-center">
            {item.name}
          </p>
        </button>
      </div>

      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={closeModal}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto">
            <div className={item.category == "Weapons" ? "flex min-h-full items-start justify-center p-4 py-20 text-center" :
              "flex min-h-full items-center justify-center p-4 text-center"}>
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95">
                <Dialog.Panel className="w-full max-w-lg transform rounded-2xl bg-slate-800 p-6 text-left align-middle shadow-xl transition-all">
                  <Dialog.Title
                    as="h3"
                    className="text-lg font-medium leading-6">
                    {item.name}
                  </Dialog.Title>
                  <Dialog.Title
                    className="font-small leading-6 text-gray-300">
                    {item.category != "Weapons" ? item.type :
                      item.type + ' - ' + "Weight: " + insertDecimalBy1(item.weight)}
                  </Dialog.Title>

                  <div className="mt-2">
                    <p className="text-sm text-gray-100">
                      {item.location}
                    </p>
                  </div>

                  {item.category != "Weapons" ? null :
                    <div className="mt-2">
                      Stats Required:
                      {Object.keys(item.stats_requirements).map((i, idx) => (
                        <p key={idx} className="text-sm text-gray-100">
                          {i + ": " + item.stats_requirements[i]}
                        </p>
                      ))}
                      <div className="grid grid-cols-2 gap-2">
                        <AffinityDropdown
                          affinityList={item.affinity}
                          affinitySelected={setAffinity} />
                        <LevelDropdown
                          levelList={item.upgrade_material}
                          levelSelected={setLevel} />
                      </div>
                      <AowDropdown
                        checkAffinity={affinity}
                        aowList={item.possible_aows}
                        aowSelected={setAow} />
                    </div>
                  }

                  <div className="mt-4">
                    <button
                      type="button"
                      className="inline-flex justify-center rounded-md border border-transparent bg-blue-100 px-4 py-2 text-sm font-medium text-blue-900 hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2"
                      onClick={addToCart}>
                      Add to cart
                    </button>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  )
}
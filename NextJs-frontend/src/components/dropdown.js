'use client'
import { Fragment, useEffect, useState } from 'react'
import { Listbox, Transition } from '@headlessui/react'
import Image from 'next/image'

const stack_logo = "/stack.png"
const checkmark_logo = "/checkmark.png"

export default function CategoryDropdown({ category, dropdownEnabled, itemTypeSelected }) {
    var type_list = []

    switch (category) {
        case "Weapons":
            type_list = [
                { type: 'Weapons' },
                { type: 'Daggers' },
                { type: 'Straight Swords' },
                { type: 'Greatswords' },
                { type: 'Colossal Swords' },
                { type: 'Thrusting Swords' },
                { type: 'Heavy Thrusting Swords' },
                { type: 'Curved Swords' },
                { type: 'Curved Greatswords' },
                { type: 'Katanas' },
                { type: 'Twinblades' },
                { type: 'Axes' },
                { type: 'Greataxes' },
                { type: 'Hammers' },
                { type: 'Greataxes' },
                { type: 'Flails' },
                { type: 'Great Hammers' },
                { type: 'Colossal Weapons' },
                { type: 'Spears' },
                { type: 'Great Spears' },
                { type: 'Halberds' },
                { type: 'Reapers' },
                { type: 'Whips' },
                { type: 'Fists' },
                { type: 'Claws' },
                { type: 'Light Bows' },
                { type: 'Bows' },
                { type: 'Greatbows' },
                { type: 'Crossbows' },
                { type: 'Ballistas' },
                { type: 'Glintstone Staffs' },
                { type: 'Sacred Seals' },
                { type: 'Torches' },
                { type: 'Small Shields' },
                { type: 'Medium Shields' },
                { type: 'Greatshields' }
            ]
            break
        case "Spells":
            type_list = [
                { type: 'Spells' },
                { type: 'Incantations' },
                { type: 'Sorceries' }
            ]
            break
        case "Consumables":
            type_list = [
                { type: 'Consumables' },
                { type: 'Ammunitions' },
                { type: 'Aromatics' },
                { type: 'Edibles' },
                { type: 'Greases' },
                { type: 'Pots' },
                { type: 'Runes' },
                { type: 'Throwables' },
                { type: 'Utility' }
            ]
            break
        case "Armors":
            type_list = [
                { type: "Armors" },
                { type: 'Helms' },
                { type: 'Chest Armors' },
                { type: 'Gauntlets' },
                { type: 'Legs' }
            ]
            break
        default:
            type_list = [
                { type: "-" }
            ]
    }

    const [selected, setSelected] = useState(type_list[0])

    function setItemTypeSelected(e) {
        setSelected(e)
        itemTypeSelected(e.type)
    }

    return (
        <>
            {!dropdownEnabled ? null :
                <div className="w-full max-w-xl px-16 pb-8 sm:px-0">
                    <Listbox value={selected} by="type" onChange={setItemTypeSelected}>
                        <div className="relative mt-1">
                            <Listbox.Button className="relative w-full cursor-default rounded-lg bg-yellow-500 py-2 pl-3 pr-10 text-left shadow-md focus:outline-none focus-visible:border-indigo-500 focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-orange-300 sm:text-sm">
                                <span className="block truncate">{selected.type}</span>
                                <span className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
                                    <Image
                                        className="shrink-0 h-auto w-auto"
                                        src={stack_logo}
                                        alt='Stack_Logo'
                                        width={20}
                                        height={0}
                                    />
                                </span>
                            </Listbox.Button>
                            <Transition
                                as={Fragment}
                                leave="transition ease-in duration-100"
                                leaveFrom="opacity-100"
                                leaveTo="opacity-0">
                                <Listbox.Options className="absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-slate-800 py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                                    {type_list.map((itemType, typeId) => (
                                        <Listbox.Option
                                            key={typeId}
                                            className={({ active }) =>
                                                `relative cursor-default select-none py-2 pl-10 pr-4 ${active ? 'bg-amber-100 text-yellow-500' : 'text-base'
                                                }`
                                            }
                                            value={itemType}>
                                            {({ selected }) => (
                                                <>
                                                    <span
                                                        className={`block truncate ${selected ? 'font-medium' : 'font-normal'
                                                            }`}>
                                                        {itemType.type}
                                                    </span>
                                                    {selected ? (
                                                        <span className="absolute inset-y-0 left-0 flex items-center pl-3 text-amber-600">
                                                            <Image
                                                                className="shrink-0 h-auto w-auto"
                                                                src={checkmark_logo}
                                                                alt='Checkmark_Logo'
                                                                width={20}
                                                                height={0}
                                                            />
                                                        </span>
                                                    ) : null}
                                                </>
                                            )}
                                        </Listbox.Option>
                                    ))}
                                </Listbox.Options>
                            </Transition>
                        </div>
                    </Listbox>
                </div>}
        </>
    )
}

export function LevelDropdown({ levelList, levelSelected }) {
    var type_list = []

    switch (levelList) {
        case "Smithing Stone":
            type_list = [
                { type: 'Base' },
                { type: 'Max' },
                { type: '+1' },
                { type: '+2' },
                { type: '+3' },
                { type: '+4' },
                { type: '+5' },
                { type: '+6' },
                { type: '+7' },
                { type: '+8' },
                { type: '+9' },
                { type: '+10' },
                { type: '+11' },
                { type: '+12' },
                { type: '+13' },
                { type: '+14' },
                { type: '+15' },
                { type: '+16' },
                { type: '+17' },
                { type: '+18' },
                { type: '+19' },
                { type: '+20' },
                { type: '+21' },
                { type: '+22' },
                { type: '+23' },
                { type: '+24' },
            ]
            break
        case "Somber Smithing Stone":
            type_list = [
                { type: 'Base' },
                { type: 'Max' },
                { type: '+1' },
                { type: '+2' },
                { type: '+3' },
                { type: '+4' },
                { type: '+5' },
                { type: '+6' },
                { type: '+7' },
                { type: '+8' },
                { type: '+9' },

            ]
            break
        default:
            type_list = [
                { type: "-" }
            ]
    }

    const [selected, setSelected] = useState(type_list[0])

    function setItemTypeSelected(e) {
        setSelected(e)
        levelSelected(e.type)
    }

    return (
        <>
            <div className="w-full max-w-xl z-10">
                <Listbox value={selected} by="type" onChange={setItemTypeSelected}>
                    <div className="relative mt-1">
                        <Listbox.Button className="relative w-full cursor-default rounded-lg bg-yellow-500 py-2 pl-3 pr-10 text-left shadow-md focus:outline-none focus-visible:border-indigo-500 focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-orange-300 sm:text-sm">
                            <span className="block truncate">{selected.type}</span>
                            <span className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
                                <Image
                                    className="shrink-0 h-auto w-auto"
                                    src={stack_logo}
                                    alt='Stack_Logo'
                                    width={20}
                                    height={0}
                                />
                            </span>
                        </Listbox.Button>
                        <Transition
                            as={Fragment}
                            leave="transition ease-in duration-100"
                            leaveFrom="opacity-100"
                            leaveTo="opacity-0">
                            <Listbox.Options className="absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-slate-800 py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                                {type_list.map((itemType, typeId) => (
                                    <Listbox.Option
                                        key={typeId}
                                        className={({ active }) =>
                                            `relative cursor-default select-none py-2 pl-10 pr-4 ${active ? 'bg-amber-100 text-yellow-500' : 'text-base'
                                            }`
                                        }
                                        value={itemType}>
                                        {({ selected }) => (
                                            <>
                                                <span
                                                    className={`block truncate ${selected ? 'font-medium' : 'font-normal'
                                                        }`}>
                                                    {itemType.type}
                                                </span>
                                                {selected ? (
                                                    <span className="absolute inset-y-0 left-0 flex items-center pl-3 text-amber-600">
                                                        <Image
                                                            className="shrink-0 h-auto w-auto"
                                                            src={checkmark_logo}
                                                            alt='Checkmark_Logo'
                                                            width={20}
                                                            height={0}
                                                        />
                                                    </span>
                                                ) : null}
                                            </>
                                        )}
                                    </Listbox.Option>
                                ))}
                            </Listbox.Options>
                        </Transition>
                    </div>
                </Listbox>
            </div>
        </>
    )
}

export function AffinityDropdown({ affinityList, affinitySelected }) {
    var type_list = []

    for (let i in affinityList) {
        type_list.push({ type: affinityList[i] })
    }

    const [selected, setSelected] = useState(type_list[0])

    function setItemTypeSelected(e) {
        setSelected(e)
        affinitySelected(e.type)
    }

    return (
        <>
            <div className="w-full max-w-xl z-10">
                <Listbox value={selected} by="type" onChange={setItemTypeSelected}>
                    <div className="relative mt-1">
                        <Listbox.Button className="relative w-full cursor-default rounded-lg bg-yellow-500 py-2 pl-3 pr-10 text-left shadow-md focus:outline-none focus-visible:border-indigo-500 focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-orange-300 sm:text-sm">
                            <span className="block truncate">{selected.type}</span>
                            <span className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
                                <Image
                                    className="shrink-0 h-auto w-auto"
                                    src={stack_logo}
                                    alt='Stack_Logo'
                                    width={20}
                                    height={0}
                                />
                            </span>
                        </Listbox.Button>
                        <Transition
                            as={Fragment}
                            leave="transition ease-in duration-100"
                            leaveFrom="opacity-100"
                            leaveTo="opacity-0">
                            <Listbox.Options className="absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-slate-800 py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                                {type_list.map((itemType, typeId) => (
                                    <Listbox.Option
                                        key={typeId}
                                        className={({ active }) =>
                                            `relative cursor-default select-none py-2 pl-10 pr-4 ${active ? 'bg-amber-100 text-yellow-500' : 'text-base'
                                            }`
                                        }
                                        value={itemType}>
                                        {({ selected }) => (
                                            <>
                                                <span
                                                    className={`block truncate ${selected ? 'font-medium' : 'font-normal'
                                                        }`}>
                                                    {itemType.type}
                                                </span>
                                                {selected ? (
                                                    <span className="absolute inset-y-0 left-0 flex items-center pl-3 text-amber-600">
                                                        <Image
                                                            className="shrink-0 h-auto w-auto"
                                                            src={checkmark_logo}
                                                            alt='Checkmark_Logo'
                                                            width={20}
                                                            height={0}
                                                        />
                                                    </span>
                                                ) : null}
                                            </>
                                        )}
                                    </Listbox.Option>
                                ))}
                            </Listbox.Options>
                        </Transition>
                    </div>
                </Listbox>
            </div>
        </>
    )
}

export function AowDropdown({ checkAffinity, aowList, aowSelected }) {
    var type_list = []

    for (let i in aowList) {
        if (checkAffinity == Object.keys(aowList[i])[0]) {
            if (Object.values(aowList[i])[0].length) {
                type_list = [
                    { type: 'Random Ash of War' }
                ]
                for (let j in Object.values(aowList[i])[0]) {
                    type_list.push({ type: aowList[i][checkAffinity][j] })
                }
            } else {
                type_list = [
                    { type: 'Standard Ash of War' }
                ]
            }
            break
        }
    }

    const [selected, setSelected] = useState(type_list[0])

    useEffect(() => {
        setSelected(type_list[0])
        type_list[0].type == "Random Ash of War" || type_list[0].type == "Standard Ash of War" ? aowSelected("None") : aowSelected(type_list[0].type)
    }, [checkAffinity])

    function setItemTypeSelected(e) {
        setSelected(e)
        e.type == "Random Ash of War" || e.type == "Standard Ash of War" ? aowSelected("None") : aowSelected(e.type)
    }

    return (
        <>
            <div className="w-full max-w-xl">
                <Listbox value={selected} by="type" onChange={setItemTypeSelected}>
                    <div className="relative mt-1">
                        <Listbox.Button className="relative w-full cursor-default rounded-lg bg-yellow-500 py-2 pl-3 pr-10 text-left shadow-md focus:outline-none focus-visible:border-indigo-500 focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-orange-300 sm:text-sm">
                            <span className="block truncate">{selected.type}</span>
                            <span className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
                                <Image
                                    className="shrink-0 h-auto w-auto"
                                    src={stack_logo}
                                    alt='Stack_Logo'
                                    width={20}
                                    height={0}
                                />
                            </span>
                        </Listbox.Button>
                        <Transition
                            as={Fragment}
                            leave="transition ease-in duration-100"
                            leaveFrom="opacity-100"
                            leaveTo="opacity-0">
                            <Listbox.Options className="absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-slate-800 py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                                {type_list.map((itemType, typeId) => (
                                    <Listbox.Option
                                        key={typeId}
                                        className={({ active }) =>
                                            `relative cursor-default select-none py-2 pl-10 pr-4 ${active ? 'bg-amber-100 text-yellow-500' : 'text-base'
                                            }`
                                        }
                                        value={itemType}>
                                        {({ selected }) => (
                                            <>
                                                <span
                                                    className={`block truncate ${selected ? 'font-medium' : 'font-normal'
                                                        }`}>
                                                    {itemType.type}
                                                </span>
                                                {selected ? (
                                                    <span className="absolute inset-y-0 left-0 flex items-center pl-3 text-amber-600">
                                                        <Image
                                                            className="shrink-0 h-auto w-auto"
                                                            src={checkmark_logo}
                                                            alt='Checkmark_Logo'
                                                            width={20}
                                                            height={0}
                                                        />
                                                    </span>
                                                ) : null}
                                            </>
                                        )}
                                    </Listbox.Option>
                                ))}
                            </Listbox.Options>
                        </Transition>
                    </div>
                </Listbox>
            </div>
        </>
    )
}

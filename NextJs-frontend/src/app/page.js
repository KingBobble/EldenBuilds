import Link from 'next/link'
import Nav from '@/components/nav'
import Category from "@/components/category"
import Banner from "@/components/banner"

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between pt-24 pb-12 overflow-hidden">
      <Nav />

      <Banner />

      <div className="mb-32 grid text-center grid-cols-2 lg:mb-0 lg:grid-cols-4 lg:text-left">
        <Link href={process.env.BUILDS_LINK}>
          <Category category="Builds"
            desc="Look at the unique and current builds for PvE and PvP."
          />
        </Link>
        <Link href="/weapons">
          <Category category="Weapons"
            desc="Every single weapon in stock to choose from."
          />
        </Link>
        <Link href="/armors">
          <Category category="Armors"
            desc="To become Elden Lord, You will need that Elden drip."
          />
        </Link>
        <Link href="/talismans">
          <Category category="Talismans"
            desc="Discover which bonus effects suits you best."
          />
        </Link>
        <Link href="/ash%20of%20wars">
          <Category category="Ash of Wars"
            desc="Explore which special abilities has to offer."
          />
        </Link>
        <Link href="/crafting%20materials">
          <Category category="Crafting Materials"
            desc="Time to collect ingredients and become a master crafter."
          />
        </Link>
        <Link href="/consumables">
          <Category category="Consumables"
            desc="Learn about every type of usable item in the game."
          />
        </Link>
        <Link href="/spells">
          <Category category="Spells"
            desc="Study all the incantations and sorceries and blast enemies away."
          />
        </Link>
        <Link href={process.env.DISCORD_LINK}>
          <Category category="Join the Discord"
            desc="Check out the builds, ask for item drops, or leave feedback."
          />
        </Link>
      </div>
    </main>
  )
}

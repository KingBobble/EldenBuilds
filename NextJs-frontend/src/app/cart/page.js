'use client'
import Nav from '@/components/nav'
import List from '@/components/cart-list'
import { useState } from 'react'

export default function Cart() {
  const [cart, removeCart] = useState(0)

  return (
    <main className="flex min-h-screen flex-col items-center pt-12 pb-32 overflow-hidden">
      <Nav cartStatus={cart} />
      <List removeItem={removeCart} />
    </main>
  )
}
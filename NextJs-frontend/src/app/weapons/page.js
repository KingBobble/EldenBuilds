import Overview from '@/components/overview'

const image_url = process.env.MEDIA_HOST

export default async function Weapons() {
  var msg = {}
  var data, status, error

  try {
    data = await GetAllWeaponsInfo()
    if ('detail' in data) {
      status = true
      if (data.detail.status == "error") {
        error = data.error
      }
    } else if ('status' in data) {
      status = true
      if (data.status == "error") {
        error = data.error
      } else {
        data = data.data
      }
    }
  } catch {
    status = false
  }

  msg = { data: data, status: status, error: error }

  return (
    <main className="flex min-h-screen flex-col items-center pt-24 pb-32 overflow-hidden">
      <Overview info={msg}
        src={image_url}
        dropdownEnabled={true}
      />
    </main>
  )
}

export async function GetAllWeaponsInfo() {
  const res = await fetch(process.env.API_HOST + '/api/GetAllItemsInfo', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ category: "WEAPONS" }),
  })

  return res.json()
}
from pydantic import BaseModel, validator


class Item(BaseModel):
    category: str
    name: str | None = None
    type: str | None = None
    weight: float | None = None

    @validator('category')
    def category_match(cls, msg):
        if not msg in ['WEAPONS', 'TALISMANS', 'SPELLS', 'MATERIALS', 'CONSUMABLES', 'AOWS', 'ARMORS']:
            raise ValueError("category must be set to WEAPONS, TALISMANS, SPELLS, MATERIALS, CONSUMABLES, AOWS, or ARMORS")
        return msg


class AddItem(BaseModel):
    category: str
    name: str
    alt: str | None = None
    type: str | None = None
    weight: float | None = None
    description: list[str] | None = None
    location: str | None = None
    src: str | None = None
    stats_requirements: list[str] | None = None
    affinity: list[str] | None = None
    upgrade_material: str | None = None
    possible_aows: list[str] | None = None

import uvicorn
import logging
from fastapi import FastAPI, Request, HTTPException
from fastapi.staticfiles import StaticFiles
import database
from config import config_path
import models
import json


app = FastAPI(
    docs_url=None,  # Disable docs API
    redoc_url=None  # Disable redoc API
)


def convert_simple_info_to_json(tups, category):
    jsonvar = {}
    jsonvar["id"] = tups[0]
    jsonvar["name"] = tups[1]
    jsonvar["alt"] = tups[2]
    jsonvar["type"] = tups[3]
    if category == "WEAPONS":
        jsonvar["weight"] = float(tups[4])
        jsonvar["description"] = json.loads(tups[5])["description"]
        jsonvar["location"] = tups[6]
        jsonvar["src"] = tups[7]
        jsonvar["category"] = tups[8]
        jsonvar["stats_requirements"] = tups[9]["requirements"]
        jsonvar["affinity"] = tups[10]["affinity"]
        jsonvar["upgrade_material"] = tups[11]
        jsonvar["possible_aows"] = tups[12]["possible_aows"]
    elif category == "TALISMANS" or category == "ARMORS":
        jsonvar["weight"] = float(tups[4])
        jsonvar["description"] = tups[5]
        jsonvar["location"] = tups[6]
        jsonvar["src"] = tups[7]
        jsonvar["category"] = tups[8]
    elif category == "SPELLS" or category == "CONSUMABLES" or category == "MATERIALS" \
        or category == "AOWS":
        jsonvar["description"] = tups[4]
        jsonvar["location"] = tups[5]
        jsonvar["src"] = tups[6]
        jsonvar["category"] = tups[7]

    return jsonvar


@app.post("/api/GetAllItemsInfo")
def get_all_items_info(item: models.Item):
    returnmessage = {"status": "success",
                     "error": "", "messages": [], "data": []}

    item_response = database.get_all_items(item)
    
    if item_response is None:
        returnmessage["status"] = "error"
        returnmessage["error"] = "DATA_ERROR"
        returnmessage["messages"].append("Data error has occurred.")
        raise HTTPException(status_code=400, detail=returnmessage)

    for items in item_response:
        item_json = convert_simple_info_to_json(items, item.category)
        returnmessage["data"].append(item_json)

    return returnmessage


"""
@app.post("/api/AddItemInfo")
def add_item_info(item: models.AddItem):
    returnmessage = {"status": "success",
                     "error": "", "messages": [], "data": []}

    status = database.add_item(item.category, item.name, item.weight, item.description, item.location)

    if not status:
        returnmessage["status"] = "error"
        returnmessage["error"] = "DATA_ERROR"
        returnmessage["messages"].append("Data not able to be added to the database.")
        raise HTTPException(status_code=400, detail=returnmessage)

    return returnmessage
"""


DATAPATH = config_path()
STATIC_PATH = DATAPATH["static_path"]
app.mount("/api", StaticFiles(directory=STATIC_PATH), name="static")


if __name__ == '__main__':
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    
    """Run main program with command: uvicorn main:app --host 127.0.0.1 --port 8000 --workers 4

    main: the file main.py (the Python "module").
    app: the object created inside of main.py with the line app = FastAPI().
    --reload: make the server restart after code changes. Only use for development.
    
    Interactive API documentation: http://127.0.0.1:8000/docs

    Gunicorn + Uvicorn combo command:

    gunicorn main:app --workers 4 --worker-class uvicorn.workers.UvicornWorker --bind 127.0.0.1:8000
    """
    
    # localhost
    uvicorn.run("main:app", host="127.0.0.1", port=8000)

    # private ip address
    # uvicorn.run("main:app", host="0.0.0.0", port=8000)#, forwarded_allow_ips='*', reload=True)

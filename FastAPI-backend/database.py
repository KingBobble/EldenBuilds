import logging
import psycopg2
#import pandas as pd
import models
from config import config_db
import json


def check_db_version():
    """ Connect to the PostgreSQL database server """
    conn = None
    
    try:
        # read connection parameters
        params = config_db()

        # connect to the PostgreSQL server
        logging.debug('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params)
    
        # create a cursor
        cur = conn.cursor()
            
        # execute a statement
        logging.debug('PostgreSQL database version:')
        cur.execute('SELECT version()')

        # display the PostgreSQL database server version
        db_version = cur.fetchone()
        logging.debug(db_version)
        
        # close the communication with the PostgreSQL
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.exception(error)
    finally:
        if conn is not None:
            conn.close()
            logging.debug('Database connection closed.')


def create_tables():
    """ Create tables in the PostgreSQL database"""
    commands = (
        """
        CREATE TABLE IF NOT EXISTS public.weapons (
            weapon_id SERIAL PRIMARY KEY,
            weapon_name VARCHAR(255) NOT NULL,
            weapon_altname VARCHAR(255) NOT NULL,
            weapon_type VARCHAR(255) NOT NULL,
            weapon_weight REAL NOT NULL,
            weapon_desc VARCHAR(1024) NOT NULL,
            weapon_location VARCHAR(512) NOT NULL,
            weapon_image VARCHAR(255) NOT NULL,
            category VARCHAR(255) NOT NULL DEFAULT 'Weapons',
            stats_requirements JSON NOT NULL DEFAULT '{ "requirements": {} }',
            affinity JSON NOT NULL DEFAULT '{ "affinity": [] }',
            upgrade_material VARCHAR(255) NOT NULL,
            possible_aows JSON NOT NULL DEFAULT '{ "possible_aows": [] }',
            weapon_status BOOL NOT NULL
        )
        """,
        """
        CREATE TABLE IF NOT EXISTS public.talismans (
            talisman_id SERIAL PRIMARY KEY,
            talisman_name VARCHAR(255) NOT NULL,
            talisman_altname VARCHAR(255) NOT NULL,
            talisman_type VARCHAR(255) NOT NULL,
            talisman_weight REAL NOT NULL,
            talisman_desc VARCHAR(1024) NOT NULL,
            talisman_location VARCHAR(512) NOT NULL,
            talisman_image VARCHAR(255) NOT NULL,
            category VARCHAR(255) NOT NULL DEFAULT 'Talismans',
            talisman_status BOOL NOT NULL
        )
        """,
        """ 
        CREATE TABLE IF NOT EXISTS public.spells (
            spell_id SERIAL PRIMARY KEY,
            spell_name VARCHAR(255) NOT NULL,
            spell_altname VARCHAR(255) NOT NULL,
            spell_type VARCHAR(255) NOT NULL,
            spell_desc VARCHAR(1024) NOT NULL,
            spell_location VARCHAR(512) NOT NULL,
            spell_image VARCHAR(255) NOT NULL,
            category VARCHAR(255) NOT NULL DEFAULT 'Spells',
            spell_status BOOL NOT NULL
        )
        """,
        """
        CREATE TABLE IF NOT EXISTS public.crafting_materials (
            material_id SERIAL PRIMARY KEY,
            material_name VARCHAR(255) NOT NULL,
            material_altname VARCHAR(255) NOT NULL,
            material_type VARCHAR(255) NOT NULL,
            material_desc VARCHAR(1024) NOT NULL,
            material_location VARCHAR(1024) NOT NULL,
            material_image VARCHAR(255) NOT NULL,
            category VARCHAR(255) NOT NULL DEFAULT 'Materials',
            material_status BOOL NOT NULL
        )
        """,
        """
        CREATE TABLE IF NOT EXISTS public.consumables (
            consumable_id SERIAL PRIMARY KEY,
            consumable_name VARCHAR(255) NOT NULL,
            consumable_altname VARCHAR(255) NOT NULL,
            consumable_type VARCHAR(255) NOT NULL,
            consumable_desc VARCHAR(1024) NOT NULL,
            consumable_location VARCHAR(1024) NOT NULL,
            consumable_image VARCHAR(255) NOT NULL,
            category VARCHAR(255) NOT NULL DEFAULT 'Consumables',
            consumable_status BOOL NOT NULL
        )
        """,
        """
        CREATE TABLE IF NOT EXISTS public.ash_of_wars (
            aow_id SERIAL PRIMARY KEY,
            aow_name VARCHAR(255) NOT NULL,
            aow_altname VARCHAR(255) NOT NULL,
            aow_type VARCHAR(255) NOT NULL,
            aow_desc VARCHAR(1024) NOT NULL,
            aow_location VARCHAR(1024) NOT NULL,
            aow_image VARCHAR(255) NOT NULL,
            category VARCHAR(255) NOT NULL DEFAULT 'AoWs',
            aow_status BOOL NOT NULL
        )
        """,
        """
        CREATE TABLE IF NOT EXISTS public.armors (
            armor_id SERIAL PRIMARY KEY,
            armor_name VARCHAR(255) NOT NULL,
            armor_altname VARCHAR(255) NOT NULL,
            armor_type VARCHAR(255) NOT NULL,
            armor_weight REAL NOT NULL,
            armor_desc VARCHAR(1024) NOT NULL,
            armor_location VARCHAR(512) NOT NULL,
            armor_image VARCHAR(255) NOT NULL,
            category VARCHAR(255) NOT NULL DEFAULT 'Armors',
            armor_status BOOL NOT NULL
        )
        """)

    conn = None

    try:
        # read the connection parameters
        params = config_db()
        
        # connect to the PostgreSQL server
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        
        # create table one by one
        for command in commands:
            cur.execute(command)
        
        # close communication with the PostgreSQL database server
        cur.close()
        
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.exception(error)
    finally:
        if conn is not None:
            conn.close()


def add_item_list():
    """
    WEAPONS
    0 - id
    1 -name
    2 - alt
    3 - type
    4 - weight
    5 - desciption
    6 - location
    7 - src
    8 - requirements
    9 - affinity
    10 - upgrade_material
    11 - possible_aows
    TALISMANS, ARMORS
    0 - id
    1 -name
    2 - alt
    3 - type
    4 - weight
    5 - desciption
    6 - location
    7 - src
    SPELLS, CONSUMABLES, MATERIALS, AOWS 
    0 - id
    1 -name
    2 - alt
    3 - type
    4 - desciption
    5 - location
    6 - src
    """
    buf = []
    item = models.AddItem
    # Config for excel
    weight = True
    item.category = "WEAPONS"
    dataframe1 = pd.read_excel("WEAPONS_v2.xlsx")
    buf = dataframe1.values

    for i in buf:
        item.name = i[1]
        item.alt = i[2]
        item.type = i[3]
        if weight:
            if pd.isna(i[4]):
                item.weight = 0.0
            else:
                item.weight = i[4]
            if pd.isna(i[5]):
                item.description = ""
            else:
                item.description = i[5]
            item.location = i[6]
            item.src = i[7]
            if item.category == "WEAPONS":
                item.stats_requirements = i[8]
                item.affinity = i[9]
                item.upgrade_material = i[10]
                item.possible_aows = i[11]
        else:
            if pd.isna(i[4]):
                item.description = ""
            item.location = i[5]
            item.src = i[6]

        add_item(item)


def add_item(item):
    """ insert a new item into the any of the category tables """
    conn = None
    category_id = None

    try:
        # read database configuration
        params = config_db()

        # connect to the PostgreSQL database
        conn = psycopg2.connect(**params)

        # create a new cursor
        cur = conn.cursor()

        if item.category == "WEAPONS":
            command = """SELECT * FROM public.weapons WHERE weapon_name=%s;"""
            cur.execute(command, (item.name,))
            exists = cur.fetchone()
            if exists:
                # execute the UPDATE statement
                sql = """UPDATE public.weapons SET weapon_desc=%s, weapon_location=%s, weapon_image=%s, category=%s, 
                    stats_requirements=%s, affinity=%s, upgrade_material=%s WHERE weapon_name=%s RETURNING weapon_id;"""
                cur.execute(sql, (item.description, item.location, item.src, item.category, item.stats_requirements,
                                  item.affinity, item.upgrade_material, item.name,))
                # get the generated id back
                category_id = cur.fetchone()[0]
            else:
                # execute the INSERT statement
                sql = """INSERT INTO public.weapons
                    VALUES(DEFAULT, %s, %s, %s, %s, %s, %s, %s, DEFAULT, %s, %s, %s, %s, TRUE) RETURNING weapon_id;"""
                cur.execute(sql, (item.name, item.alt, item.type, item.weight, item.description, item.location, 
                                  item.src, item.stats_requirements, item.affinity, item.upgrade_material, item.possible_aows,))

                # get the generated id back
                category_id = cur.fetchone()[0]
        elif item.category == "TALISMANS":
            command = """SELECT * FROM public.talismans WHERE talisman_name=%s;"""
            cur.execute(command, (item.name,))
            exists = cur.fetchone()
            if exists:
                # execute the UPDATE statement
                sql = """UPDATE public.talismans SET talisman_weight=%s, talisman_desc=%s WHERE talisman_name=%s 
                    RETURNING talisman_id;"""
                cur.execute(sql, (item.weight, item.description, item.name,))
                # get the generated id back
                category_id = cur.fetchone()[0]
            else:
                # execute the INSERT statement
                sql = """INSERT INTO public.talismans VALUES(DEFAULT, %s, %s, %s, %s, %s, %s, %s, DEFAULT, TRUE) 
                    RETURNING talisman_id;"""
                cur.execute(sql, (item.name, item.alt, item.type, item.weight, item.description, 
                                  item.location, item.src,))

                # get the generated id back
                category_id = cur.fetchone()[0]
        elif item.category == "SPELLS":
            command = """SELECT * FROM public.spells WHERE spell_name=%s"""
            cur.execute(command, (item.name,))
            exists = cur.fetchone()
            if exists:
                # execute the UPDATE statement
                sql = """UPDATE public.spells SET spell_desc=%s WHERE spell_name=%s 
                    RETURNING spell_id;"""
                cur.execute(sql, (item.description, item.name,))
                # get the generated id back
                category_id = cur.fetchone()[0]
            else:
                # execute the INSERT statement
                sql = """INSERT INTO public.spells
                    VALUES(DEFAULT, %s, %s, %s, %s, %s, %s, DEFAULT, TRUE) RETURNING spell_id;"""
                cur.execute(sql, (item.name, item.alt, item.type, 
                                  item.description, item.location, item.src,))

                # get the generated id back
                category_id = cur.fetchone()[0]
        elif item.category == "MATERIALS":
            command = """SELECT * FROM public.crafting_materials WHERE material_name=%s"""
            cur.execute(command, (item.name,))
            exists = cur.fetchone()
            if exists:
                # execute the UPDATE statement
                sql = """UPDATE public.crafting_materials SET material_desc=%s WHERE material_name=%s 
                    RETURNING material_id;"""
                cur.execute(sql, (item.description, item.name,))
                # get the generated id back
                category_id = cur.fetchone()[0]
            else:
                # execute the INSERT statement
                sql = """INSERT INTO public.crafting_materials
                    VALUES(DEFAULT, %s, %s, %s, %s, %s, %s, DEFAULT, TRUE) RETURNING material_id;"""
                cur.execute(sql, (item.name, item.alt, item.type, item.description, 
                                  item.location, item.src,))

                # get the generated id back
                category_id = cur.fetchone()[0]
        elif item.category == "CONSUMABLES":
            command = """SELECT * FROM public.consumables WHERE consumable_name=%s"""
            cur.execute(command, (item.name,))
            exists = cur.fetchone()
            if exists:
                # execute the UPDATE statement
                sql = """UPDATE public.consumables SET consumable_desc=%s WHERE consumable_name=%s 
                    RETURNING consumable_id;"""
                cur.execute(sql, (item.description, item.name,))
                # get the generated id back
                category_id = cur.fetchone()[0]
            else:
                # execute the INSERT statement
                sql = """INSERT INTO public.consumables
                    VALUES(DEFAULT, %s, %s, %s, %s, %s, %s, DEFAULT, TRUE) RETURNING consumable_id;"""
                cur.execute(sql, (item.name, item.alt, item.type, 
                                  item.description, item.location, item.src,))

                # get the generated id back
                category_id = cur.fetchone()[0]
        elif item.category == "AOWS":
            command = """SELECT * FROM public.ash_of_wars WHERE aow_name=%s"""
            cur.execute(command, (item.name,))
            exists = cur.fetchone()
            if exists:
                # execute the UPDATE statement
                sql = """UPDATE public.ash_of_wars SET aow_desc=%s WHERE aow_name=%s 
                    RETURNING aow_id;"""
                cur.execute(sql, (item.description, item.name,))
                # get the generated id back
                category_id = cur.fetchone()[0]
            else:
                # execute the INSERT statement
                sql = """INSERT INTO public.ash_of_wars
                    VALUES(DEFAULT, %s, %s, %s, %s, %s, %s, DEFAULT, TRUE) RETURNING aow_id;"""
                cur.execute(sql, (item.name, item.alt, item.type, item.description, 
                                  item.location, item.src,))

                # get the generated id back
                category_id = cur.fetchone()[0]
        elif item.category == "ARMORS":
            command = """SELECT * FROM public.armors WHERE armor_name=%s;"""
            cur.execute(command, (item.name,))
            exists = cur.fetchone()
            if exists:
                # execute the UPDATE statement
                sql = """UPDATE public.armors SET armor_weight=%s, armor_desc=%s WHERE armor_name=%s 
                    RETURNING armor_id;"""
                cur.execute(sql, (item.weight, item.description, item.name,))
                # get the generated id back
                category_id = cur.fetchone()[0]
            else:
                # execute the INSERT statement
                sql = """INSERT INTO public.armors
                    VALUES(DEFAULT, %s, %s, %s, %s, %s, %s, %s, DEFAULT, TRUE) RETURNING armor_id;"""
                cur.execute(sql, (item.name, item.alt, item.type,
                                item.weight, item.description, item.location,
                                item.src,))

                # get the generated id back
                category_id = cur.fetchone()[0]

        # commit the changes to the database
        conn.commit()

        # close communication with the database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.exception(error)
    finally:
        if conn is not None:
            conn.close()

    return category_id


def get_all_items(item):
    conn = None
    results = None

    try:
        # read database configuration
        params = config_db()

        # connect to the PostgreSQL database
        conn = psycopg2.connect(**params)

        # create a new cursor
        cur = conn.cursor()

        if item.category == "WEAPONS":
            if item.name:
                command = """SELECT * FROM public.weapons WHERE LOWER(weapon_name)=LOWER(%s);"""
                cur.execute(command, (item.name,))
                results = cur.fetchall()
            elif item.type:
                command = """SELECT * FROM public.weapons WHERE LOWER(weapon_type)=LOWER(%s) ORDER BY weapon_id ASC;"""
                cur.execute(command, (item.type,))
                results = cur.fetchall()
            else:
                command = """SELECT * FROM public.weapons ORDER BY weapon_id ASC;"""
                cur.execute(command)
                results = cur.fetchall()
        elif item.category == "TALISMANS":
            if item.name:
                command = """SELECT * FROM public.talismans WHERE LOWER(talisman_name)=LOWER(%s);"""
                cur.execute(command, (item.name,))
                results = cur.fetchall()
            elif item.type:
                command = """SELECT * FROM public.talismans WHERE LOWER(talisman_type)=LOWER(%s) ORDER BY talisman_id ASC;"""
                cur.execute(command, (item.type,))
                results = cur.fetchall()
            else:
                command = """SELECT * FROM public.talismans ORDER BY talisman_id ASC;"""
                cur.execute(command)
                results = cur.fetchall()
        elif item.category == "SPELLS":
            if item.name:
                command = """SELECT * FROM public.spells WHERE LOWER(spell_name)=LOWER(%s);"""
                cur.execute(command, (item.name,))
                results = cur.fetchall()
            elif item.type:
                command = """SELECT * FROM public.spells WHERE LOWER(spell_type)=LOWER(%s) ORDER BY spell_id ASC;"""
                cur.execute(command, (item.type,))
                results = cur.fetchall()
            else:
                command = """SELECT * FROM public.spells ORDER BY spell_id ASC;"""
                cur.execute(command)
                results = cur.fetchall()
        elif item.category == "MATERIALS":
            if item.name:
                command = """SELECT * FROM public.crafting_materials WHERE LOWER(material_name)=LOWER(%s);"""
                cur.execute(command, (item.name,))
                results = cur.fetchall()
            elif item.type:
                command = """SELECT * FROM public.crafting_materials WHERE LOWER(material_type)=LOWER(%s) ORDER BY material_id ASC;"""
                cur.execute(command, (item.type,))
                results = cur.fetchall()
            else:
                command = """SELECT * FROM public.crafting_materials ORDER BY material_id ASC;"""
                cur.execute(command)
                results = cur.fetchall()
        elif item.category == "CONSUMABLES":
            if item.name:
                command = """SELECT * FROM public.consumables WHERE LOWER(consumable_name)=LOWER(%s);"""
                cur.execute(command, (item.name,))
                results = cur.fetchall()
            elif item.type:
                command = """SELECT * FROM public.consumables WHERE LOWER(consumable_type)=LOWER(%s) ORDER BY consumable_id ASC;"""
                cur.execute(command, (item.type,))
                results = cur.fetchall()
            else:
                command = """SELECT * FROM public.consumables ORDER BY consumable_id ASC;"""
                cur.execute(command)
                results = cur.fetchall()
        elif item.category == "AOWS":
            if item.name:
                command = """SELECT * FROM public.ash_of_wars WHERE LOWER(aow_name)=LOWER(%s);"""
                cur.execute(command, (item.name,))
                results = cur.fetchall()
            elif item.type:
                command = """SELECT * FROM public.ash_of_wars WHERE LOWER(aow_type)=LOWER(%s) ORDER BY aow_id ASC;"""
                cur.execute(command, (item.type,))
                results = cur.fetchall()
            else:
                command = """SELECT * FROM public.ash_of_wars ORDER BY aow_id ASC;"""
                cur.execute(command)
                results = cur.fetchall()
        elif item.category == "ARMORS":
            if item.name:
                command = """SELECT * FROM public.armors WHERE LOWER(armor_name)=LOWER(%s);"""
                cur.execute(command, (item.name,))
                results = cur.fetchall()
            elif item.type:
                command = """SELECT * FROM public.armors WHERE LOWER(armor_type)=LOWER(%s) ORDER BY armor_id ASC;"""
                cur.execute(command, (item.type,))
                results = cur.fetchall()
            else:
                command = """SELECT * FROM public.armors ORDER BY armor_id ASC;"""
                cur.execute(command)
                results = cur.fetchall()

    # close communication with the database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.exception(error)
    finally:
        if conn is not None:
            conn.close()

    return results

if __name__ == '__main__':
    print("STARTING...")
    #create_tables()
    #add_item_list()